app.factory('CacheService', function($http, $rootScope, $log) {
    var envs = [];
    var env = {"id": 0, "label": "LOADING"};

    return {
        reset: function() {
        },
        getEnv: function() {
            return env;
        },
        setEnv: function(newEnv) {
            env = newEnv;
        },
        setEnvs: function(allEnvs) {
            envs = allEnvs;
        }
    };
});

