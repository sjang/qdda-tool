app.factory('AssessmentService', function($http, $rootScope, $log, CacheService) {
    var plans = [];
    var plan = {};
    var selectedSection = {};

    // instantiate our initial object
    var AssessmentService = function() {
    };

    AssessmentService.prototype.getAllPlans = function() {
        return plans;
    };
    AssessmentService.prototype.get = function() {
        return plan;
    };
    AssessmentService.prototype.getSections = function() {
        return plan.sections;
    };
    AssessmentService.prototype.getSelectedSection = function() {
        return selectedSection;
    };
    AssessmentService.prototype.setSelectedSection = function(section) {
        selectedSection = section;
    };

    AssessmentService.prototype.getName = function() {
        return plan.name;
    };
    AssessmentService.prototype.setName = function(newName) {
        plan.name = newName;
    };
    
    AssessmentService.prototype.loadAllPlans = function() {
    	$log.debug("loading assessment plans");
        var url = CacheService.getEnv().url + "/plan";
        $log.debug(url);
        var promise = $http.get(
                url
                )
                .then(
                        function(response)
                        {
                            /*DEBUG*/$log.debug("getAccessmentPlans success");
                            var data = response.data;
                            if (data === 'null')
                            {
                                /*DEBUG*/$log.debug("NULL returned");
                                plans = [];
                            }
                            else
                                plans = data.documentPlan;
                            $rootScope.$broadcast('plansLoaded');
                            return plans;
                        }
                , function(response)
                {
                    /*DEBUG*/$log.error("http.get plans error");
                    $log.error("unable to retrieve existing Plans." + response.data);
                    plans = [];
                    return plans;
                });
        return promise;
    }
    ;


    return AssessmentService;
});