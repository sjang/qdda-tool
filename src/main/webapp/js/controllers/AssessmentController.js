app.controller('AssessmentCtrl', function($rootScope, $scope, $route, $http, $location, $log, AssessmentService) {
    var svc = new AssessmentService();
    $scope.schema = {};
    $scope.collapsed = true;
    $scope.editQuestion = false;
    init();
    function init() {
        //loadSchema();        
        svc.loadAllPlans();
    }   
    ;

    function loadSchema()
    {
        $http.get('json/plan.json')
                .success(function(response) {
                    $scope.schema = response;
                })
                .error(function(response) {
                });
    }
    ;
    
    $scope.selectSection = function(section)
    {
        svc.setSelectedSection(section);
    };
    
    $scope.selectedSection = function() { return svc.getSelectedSection(); };
    
    $scope.plans = function() { return svc.getAllPlans(); };
});