app.controller('MainController', function($rootScope, $scope, analytics, $location, $http, CacheService){
    initEnvsFromJson();
    
  $rootScope.$on("$routeChangeStart", function(){
    $rootScope.loading = true;
  });

  $rootScope.$on("$routeChangeSuccess", function(){
    $rootScope.loading = false;
  });

  var scrollItems = [];

  for (var i=1; i<=100; i++) {
    scrollItems.push("Item " + i);
  }
    function initEnvsFromJson()
    {
        $http.get('json/envs.json')
                .success(function(response) {
                    $scope.envs = response;
                    CacheService.setEnvs($scope.envs);
                    setEnv();
                })
                .error(function(response) {
                    $scope.env = {"id": -1, "label": "ERROR"};
                });
    }
    ;
    function setEnv()
    {
        var url = $location.absUrl().toLowerCase();
        for (var i = 0; i < $scope.envs.length; ++i)
        {
            if (url.indexOf("localhost") >= 0)
            {
                if ($scope.envs[i].label.indexOf("LocalHost") >= 0)
                    CacheService.setEnv($scope.envs[i]);
            }
            else if (url.indexOf("dev") >= 0)
            {
                if ($scope.envs[i].label.indexOf("Development") >= 0)
                    CacheService.setEnv($scope.envs[i]);
            }
            else if (url.indexOf("int") >= 0)
            {
                if ($scope.envs[i].label.indexOf("Integration") >= 0)
                    CacheService.setEnv($scope.envs[i]);
            }
            else if (url.indexOf("qa") >= 0)
            {
                if ($scope.envs[i].label.indexOf("Test") >= 0)
                    CacheService.setEnv($scope.envs[i]);
            }
            else
                CacheService.setEnv($scope.envs[0]);
        }
        if (CacheService.getEnv().id === 0)
            CacheService.setEnv({"id": -1, "label": "ERROR"});
        $scope.env = CacheService.getEnv();
    };

  $scope.scrollItems = scrollItems;
  $scope.invoice = {payed: true};
  
  $scope.userAgent =  navigator.userAgent;
  $scope.chatUsers = [
    { name: "Carlos  Flowers", online: true },
    { name: "Byron Taylor", online: true },
    { name: "Jana  Terry", online: true },
    { name: "Darryl  Stone", online: true },
    { name: "Fannie  Carlson", online: true },
    { name: "Holly Nguyen", online: true },
    { name: "Bill  Chavez", online: true },
    { name: "Veronica  Maxwell", online: true },
    { name: "Jessica Webster", online: true },
    { name: "Jackie  Barton", online: true },
    { name: "Crystal Drake", online: false },
    { name: "Milton  Dean", online: false },
    { name: "Joann Johnston", online: false },
    { name: "Cora  Vaughn", online: false },
    { name: "Nina  Briggs", online: false },
    { name: "Casey Turner", online: false },
    { name: "Jimmie  Wilson", online: false },
    { name: "Nathaniel Steele", online: false },
    { name: "Aubrey  Cole", online: false },
    { name: "Donnie  Summers", online: false },
    { name: "Kate  Myers", online: false },
    { name: "Priscilla Hawkins", online: false },
    { name: "Joe Barker", online: false },
    { name: "Lee Norman", online: false },
    { name: "Ebony Rice", online: false }
  ];

});